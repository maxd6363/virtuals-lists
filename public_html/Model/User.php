<?php
namespace Model;

class User
{

    private $id;
    private $userName;
    private $userPassword;
    private $userEmail;
    private $admin;
    private $checklists;

    public function __construct(?int $id, ?string $userName, ?string $userPassword, ?string $userEmail, ?bool $admin, ?array $checklists)
    {
        $this->id = $id;
        $this->userName = $userName;
        $this->userPassword = $userPassword;
        $this->userEmail = $userEmail;
        $this->admin = $admin;
        $this->checklists = $checklists;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    public function getUserPassword(): ?string
    {
        return $this->userPassword;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function isAdmin(): ?bool
    {
        return $this->admin;
    }

    public function getChecklist(): ?array
    {
        return $this->checklists;
    }

    

}
