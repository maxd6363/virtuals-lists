<?php
namespace Model;

use PDO;

class Connection extends PDO
{

    private $statement;

    public function __construct(string $database, string $username, string $password)
    {
        parent::__construct($database, $username, $password);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function executeQuery(string $query, array $parameters = []): bool
    {
        $this->statement = parent::prepare($query);
        foreach ($parameters as $name => $value) {
            $this->statement->bindValue($name, $value[0], $value[1]);
        }
        return $this->statement->execute();
    }

    public function getResults(): array
    {
        return $this->statement->fetchall();
    }

    public function getCount(): int
    {
        return $this->statement->fetchColumn();
    }

    


}

?>