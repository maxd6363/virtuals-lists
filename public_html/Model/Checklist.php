<?php

namespace Model;

class Checklist
{

    private $id;
    private $name;
    private $owner;
    private $members;
    private $items;
    // array<Item>

    public function __construct(?int $id, string $name, ?int $owner, ?array $members, array $items)
    {
        $this->id = $id;
        $this->name = $name;
        $this->owner = $owner;
        $this->members = $members;
        $this->items = $items;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOwner(): ?int
    {
        return $this->owner;
    }

    public function getMembers(): ?array
    {
        return $this->members;
    }

    public function getItems(): array
    {
        return $this->items;
    }


}

?>