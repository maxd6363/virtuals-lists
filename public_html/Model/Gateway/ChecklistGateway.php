<?php

namespace Model\Gateway;

use PDO;
use Model\Connection;
use Model\Checklist;
use Model\User;
use Model\Item;

class ChecklistGateway
{

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    // --------- INSERT ---------


    public function insert(Checklist $checklist): bool
    {
        $query = "INSERT INTO Tchecklists VALUES (:id, :title, :idOwner)";
        return $this->connection->executeQuery($query, array(
            ':id' => array($checklist->id, PDO::PARAM_INT),
            ':title' => array($checklist->title, PDO::PARAM_STR),
            ':idOwner' => array($checklist->owner, PDO::PARAM_INT)
        ));
    }

    public function addPublicCheckList($name):bool{
        
        $query = "INSERT INTO Tchecklists VALUES (null, :title, null)";
        return $this->connection->executeQuery($query, array(
            ':title' => array($name, PDO::PARAM_STR)
        ));
    }

    public function addPrivateCheckList($name,$username):bool{
        
        $query = "INSERT INTO Tchecklists VALUES (null, :title, (SELECT id FROM Tusers WHERE userName = :username))";
        return $this->connection->executeQuery($query, array(
            ':title' => array($name, PDO::PARAM_STR),
            ':username' => array($username,PDO::PARAM_STR)
        ));
    }
    

    
    // --------- FIND ---------


    public function find(int $id): array
    {
        $query = "SELECT C.id ,C.title ,C.idOwner ,I.id AS idItem FROM Tchecklists C, Titems I WHERE C.id = I.idCheckList AND C.id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function findByTitle(string $title): array
    {
        $query = "SELECT C.id ,C.title ,C.idOwner ,I.id AS idItem FROM Tchecklists C, Titems I WHERE C.id = I.idCheckList AND title = :title";
        $this->connection->executeQuery($query, array(
            ':title' => array($title, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByOwner(User $owner): array
    {
        $query = "SELECT C.id ,C.title ,C.idOwner ,I.id AS idItem FROM Tchecklists C, Titems I WHERE C.id = I.idCheckList AND C.idOwner = :idOwner";
        $this->connection->executeQuery($query, array(
            ':idOwner' => array($owner->id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function findByMember(User $member): array
    {
        $query = "SELECT C.id ,C.title ,C.idOwner ,I.id AS idItem FROM Tchecklists C, Titems I WHERE C.id = I.idCheckList AND C.id IN (SELECT idChecklist FROM Tchecklistmembers WHERE idMember = :idMember)";
        $this->connection->executeQuery($query, array(
            ':idMember' => array($member->id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function findByItem(Item $item): array
    {
        $query = "SELECT C.id ,C.title ,C.idOwner ,I.id AS idItem FROM Tchecklists C, Titems I WHERE C.id = I.idCheckList AND C.id IN (SELECT idChecklist FROM Titems WHERE id = :idItem)";
        $this->connection->executeQuery($query, array(
            ':idItem' => array($item->id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function findPublicChecklist(): array
    {
        $query = "SELECT id ,title ,idOwner FROM Tchecklists WHERE idOwner IS NULL ORDER BY id";
        $this->connection->executeQuery($query, array());
        return $this->connection->getResults();
    }

    public function findPrivateChecklist($username): array
    {
        $query = "SELECT C.id ,C.title ,C.idOwner FROM Tchecklists C, Tusers U WHERE C.idOwner = U.id AND U.userName = :username";
        $this->connection->executeQuery($query, array(
            ':username' => array($username,PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }


    public function findAllPrivateChecklist(): array
    {
        $query = "SELECT id ,title ,idOwner FROM Tchecklists WHERE idOwner IS NOT NULL ORDER BY idOwner";
        $this->connection->executeQuery($query, array());
        return $this->connection->getResults();
    }

    


    public function getNbChecklists(): int
    {
        $query = "SELECT count(*) AS count FROM Tchecklists";
        $this->connection->executeQuery($query, array());
        $res = $this->connection->getCount();
        return $res;
    }



    public function findAllItem($id): array
    {
        $query = "SELECT I.id AS id FROM Titems I, Tchecklists C WHERE I.idChecklist = C.id AND C.id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }





    // --------- UPDATE ---------


    public function updateTitle(int $id, string $title): bool
    {
        $query = "UPDATE Tchecklists SET title = :title WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':title' => array($title, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    // --------- DELETE ---------


    public function delete(int $id): bool
    {
        $query = "DELETE FROM Tchecklists WHERE id = :id";
        return $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }
}
