<?php

namespace Model\Gateway;

use Exception;
use PDO;
use Model\Connection;
use Model\User;
use Model\Checklist;


class UserGateway
{

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    // --------- INSERT ---------

    public function registerUser(string $username, string $password, string $email): bool
    {
        $query = "INSERT INTO Tusers VALUES (null, :userName, :userPassword, :userEmail, false)";
        return $this->connection->executeQuery($query, array(
            ':userName' => array($username, PDO::PARAM_STR),
            ':userPassword' => array($password, PDO::PARAM_STR),
            ':userEmail' => array($email, PDO::PARAM_STR)
        ));
    }





    public function insert(User $user): bool
    {
        $query = "INSERT INTO Tusers VALUES (:id, :userName, :userPassword, :userEmail, :isAdmin)";
        return $this->connection->executeQuery($query, array(
            ':id' => array($user->id, PDO::PARAM_INT),
            ':userName' => array($user->userName, PDO::PARAM_STR),
            ':userPassword' => array($user->userPassword, PDO::PARAM_STR),
            ':userEmail' => array($user->userEmail, PDO::PARAM_STR),
            ':isAdmin' => array($user->admin, PDO::PARAM_INT)
        ));
    }

    // --------- FIND ---------


    public function findAll(): array
    {
        $query = "SELECT * FROM Tusers";
        $this->connection->executeQuery($query, array());
        return $this->connection->getResults();
    }

    public function find(?int $id): ?User
    {
        if ($id == null) return null;
        $query = "SELECT * FROM Tusers WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults()[0];
        //return first row of getResults, because the id is unique in the database
    }

    public function findByUserName(string $userName): array
    {
        $query = "SELECT * FROM Tusers WHERE userName = :userName";
        $this->connection->executeQuery($query, array(
            ':userName' => array($userName, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByUserPassword(string $userPassword): array
    {
        $query = "SELECT * FROM Tusers WHERE userPassword = :userPassword";
        $this->connection->executeQuery($query, array(
            ':userPassword' => array($userPassword, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByEmail(string $userEmail): array
    {
        $query = "SELECT * FROM Tusers WHERE userEmail = :userEmail";
        $this->connection->executeQuery($query, array(
            ':userEmail' => array($userEmail, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByIsAdmin(bool $isAdmin): array
    {
        $query = "SELECT * FROM Tusers WHERE isAdmin = :isAdmin";
        $this->connection->executeQuery($query, array(
            ':isAdmin' => array($isAdmin, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function findByCheckList(Checklist $checklist): array
    {
        $query = "SELECT * FROM Tusers WHERE id IN (SELECT idOwner FROM TChecklists WHERE id = :idChecklist)";
        $this->connection->executeQuery($query, array(
            ':idChecklist' => array($checklist->id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function getNbUsers(): int
    {
        $query = "SELECT count(*) AS count FROM Tusers";
        $this->connection->executeQuery($query, array());
        $res = $this->connection->getCount();
        return $res;
    }

    public function getPassword($username): ?string
    {
        $query = "SELECT userPassword FROM Tusers WHERE userName = :userName";
        $this->connection->executeQuery($query, array(
            ':userName' => array($username, PDO::PARAM_STR)
        ));
        $res = $this->connection->getResults();
        if (isset($res[0])) {
            if (isset($res[0]['userPassword'])) {
                return $res[0]['userPassword'];
            }
        }
        return null;
    }


    public function isAdmin($username): ?bool
    {
        $query = "SELECT isAdmin FROM Tusers WHERE userName = :userName";
        $this->connection->executeQuery($query, array(
            ':userName' => array($username, PDO::PARAM_STR)
        ));
        $res = $this->connection->getResults();
        if (isset($res[0])) {
            if (isset($res[0]['isAdmin'])) {
                return $res[0]['isAdmin'];
            }
        }
        return null;
    }

    public function findAllUsername(): array
    {
        $query = "SELECT id, userName FROM Tusers";
        $this->connection->executeQuery($query, array());
        return $this->connection->getResults();
    }


    public function findChecklistInfoUsers(): array
    {
        $query = "SELECT (SELECT COUNT(*) FROM Tchecklists WHERE  idOwner = U.id) AS nbChecklist FROM Tusers U ORDER BY U.id";
        $this->connection->executeQuery($query, array());
        return $this->connection->getResults();
    }

    public function findItemInfoUsers(): array
    {
        $query = "SELECT (SELECT COUNT(*) FROM Titems WHERE  idCreator = U.id) AS nbItem FROM Tusers U ORDER BY U.id";
        $this->connection->executeQuery($query, array());
        return $this->connection->getResults();
    }


    public function findAllIdChecklistsForUser($idUser): array
    {
        try{
            $query = "SELECT C.id FROM Tchecklists C, Tusers U WHERE C.idOwner = U.id AND U.id = :idUser";
            $this->connection->executeQuery($query, array(
                ':idUser' => array($idUser, PDO::PARAM_INT)

            ));

        }catch(Exception $e){
            print($e->getTraceAsString());
            print_r($this->connection->errorInfo());
            
        }
        return $this->connection->getResults();
    }


    



    // --------- UPDATE ---------


    public function updateUserName(int $id, string $userName): bool
    {
        $query = "UPDATE Tusers SET userName = :userName WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':userName' => array($userName, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    public function updateUserPassword(int $id, string $userPassword): bool
    {
        $query = "UPDATE Tusers SET userPassword = :userPassword WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':userPassword' => array($userPassword, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    public function updateUserEmail(int $id, string $userEmail): bool
    {
        $query = "UPDATE Tusers SET userEmail = :userEmail WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':userEmail' => array($userEmail, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    public function updateIsAdmin(int $id, bool $isAdmin): bool
    {
        $query = "UPDATE Tusers SET isAdmin = :isAdmin WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':isAdmin' => array($isAdmin, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    // --------- DELETE ---------


    public function delete(int $id): bool
    {
        $query = "DELETE FROM Tusers WHERE id = :id"; // Dangerous
        return  $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }
}
