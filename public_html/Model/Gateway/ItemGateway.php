<?php

namespace Model\Gateway;

use PDO;
use DateTime;
use Model\Connection;
use Model\Item;



class ItemGateway
{

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    // --------- INSERT ---------


    public function insert(Item $item): bool
    {
        $query = "INSERT INTO Titems VALUES (:id, :title, :isChecked, :creationDate, :isTagged, :tagName, :tagColor, :idCreator, :idChecklist)";
        return $this->connection->executeQuery($query, array(
            ':id' => array($item->getId(), PDO::PARAM_INT),
            ':title' => array($item->getTitle(), PDO::PARAM_STR),
            ':isChecked' => array($item->isChecked(), PDO::PARAM_BOOL),
            ':creationDate' => array($item->getCreationDate(), PDO::PARAM_STR),
            ':isTagged' => array($item->isTagged(), PDO::PARAM_BOOL),
            ':tagName' => array($item->getTagText(), PDO::PARAM_STR),
            ':tagColor' => array($item->getTagColor(), PDO::PARAM_STR),
            ':idCreator' => array($item->getCreator()->getId(), PDO::PARAM_INT),
            ':idChecklist' => array($item->getIdChecklist(), PDO::PARAM_INT)
        ));
    }

    public function addPublicItem(Item $item): bool
    {
        $query = "INSERT INTO Titems VALUES (null, :title, false, SYSDATE(), :tagged, :tagName, :tagColor, null, :idChecklist)";
        return $this->connection->executeQuery($query, array(
            ':title' => array($item->getTitle(), PDO::PARAM_STR),
            ':tagged' => array($item->isTagged(), PDO::PARAM_BOOL),
            ':tagName' => array($item->getTagText(), PDO::PARAM_STR),
            ':tagColor' => array($item->getTagColor(), PDO::PARAM_STR),
            ':idChecklist' => array($item->getIdChecklist(), PDO::PARAM_INT)
        ));
    }


    public function addPrivateItem(Item $item, $username): bool
    {
        $query = "INSERT INTO Titems VALUES (null, :title, false, SYSDATE(), :tagged, :tagName, :tagColor, (SELECT id FROM Tusers WHERE userName = :username), :idChecklist)";
        return $this->connection->executeQuery($query, array(
            ':title' => array($item->getTitle(), PDO::PARAM_STR),
            ':tagged' => array($item->isTagged(), PDO::PARAM_BOOL),
            ':tagName' => array($item->getTagText(), PDO::PARAM_STR),
            ':tagColor' => array($item->getTagColor(), PDO::PARAM_STR),
            ':username' => array($username, PDO::PARAM_STR),
            ':idChecklist' => array($item->getIdChecklist(), PDO::PARAM_INT)
        ));
    }

    // --------- FIND ---------


    public function findAll(): array
    {
        $query = "SELECT * FROM Titems";
        $this->connection->executeQuery($query, array());
        return $this->connection->getResults();
    }

    public function find(int $id): array
    {
        $query = "SELECT * FROM Titems WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    public function findByTitle(string $title): array
    {
        $query = "SELECT * FROM Titems WHERE title = :title";
        $this->connection->executeQuery($query, array(
            ':title' => array($title, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByIsChecked(bool $isChecked): array
    {
        $query = "SELECT * FROM Titems WHERE checked = :checked";
        $this->connection->executeQuery($query, array(
            ':checked' => array($isChecked, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByCreationDate(DateTime $creationDate): array
    {
        $query = "SELECT * FROM Titems WHERE creationDate = :creationDate";
        $this->connection->executeQuery($query, array(
            ':creationDate' => array($creationDate, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByIsTagged(bool $isTagged): array
    {
        $query = "SELECT * FROM Titems WHERE isTagged = :isTagged";
        $this->connection->executeQuery($query, array(
            ':isTagged' => array($isTagged, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByTagText(string $tagText): array
    {
        $query = "SELECT * FROM Titems WHERE tagName = :tagName";
        $this->connection->executeQuery($query, array(
            ':tagName' => array($tagText, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByTagColor(string $tagColor): array
    {
        $query = "SELECT * FROM Titems WHERE tagColor = :tagColor";
        $this->connection->executeQuery($query, array(
            ':tagColor' => array($tagColor, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByIdUser(int $idCreator): array
    {
        $query = "SELECT * FROM Titems WHERE idCreator = :idCreator";
        $this->connection->executeQuery($query, array(
            ':creationDate' => array($idCreator, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }

    public function findByIdChecklist(int $idChecklist): array
    {
        $query = "SELECT * FROM Titems WHERE idChecklist = :idChecklist";

        $this->connection->executeQuery($query, array(
            ':idChecklist' => array($idChecklist, PDO::PARAM_STR)
        ));
        return $this->connection->getResults();
    }




    // --------- UPDATE ---------


    public function updateTitle(int $id, string $title): bool
    {
        $query = "UPDATE Titems SET title = :title WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':title' => array($title, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }



    public function updateIsTagged(int $id, bool $isTagged): bool
    {
        $query = "UPDATE Titems SET isTagged = :isTagged WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':isTagged' => array($isTagged, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    public function updateTagText(int $id, $tagText): bool
    {
        $query = "UPDATE Titems SET tagName = :tagName WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':tagName' => array($tagText, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    public function updateTagColor(int $id, string $tagColor): bool
    {
        $query = "UPDATE Titems SET tagColor = :tagColor WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':tagColor' => array($tagColor, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    public function updateIsChecked(bool $isChecked, int $idItem, int $idChecklist): bool
    {
        $query = "UPDATE Titems SET isChecked = :isChecked WHERE id = :idItem AND idChecklist = :idChecklist ";
        return  $this->connection->executeQuery($query, array(
            ':isChecked' => array($isChecked, PDO::PARAM_STR),
            ':idItem' => array($idItem, PDO::PARAM_INT),
            ':idChecklist' => array($idChecklist, PDO::PARAM_INT)
        ));
    }







    // --------- DELETE ---------


    public function delete(int $id): bool
    {
        $query = "DELETE FROM Titems WHERE id = :id";
        return  $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    public function deletePublicItem(int $idItem, int $idChecklist): bool
    {
        // idChecklist pour être sur que l'item fait bien parti de sa checklist (sécurité)
        $query = "DELETE FROM Titems WHERE id = :id AND idChecklist = :idChecklist AND idCreator IS NULL";
        return  $this->connection->executeQuery($query, array(
            ':id' => array($idItem, PDO::PARAM_INT),
            ':idChecklist' => array($idChecklist, PDO::PARAM_INT)
        ));
    }


    public function deletePrivateItem(int $idItem, int $idChecklist): bool
    {
        $query = "DELETE FROM Titems WHERE id = :id AND idChecklist = :idChecklist AND idCreator IS NOT NULL";
        return  $this->connection->executeQuery($query, array(
            ':id' => array($idItem, PDO::PARAM_INT),
            ':idChecklist' => array($idChecklist, PDO::PARAM_INT)
        ));
    }
    public function deletePrivateItemAdmin(int $idItem, int $idChecklist): bool
    {
        $query = "DELETE FROM Titems WHERE id = :id AND idChecklist = :idChecklist";
        return  $this->connection->executeQuery($query, array(
            ':id' => array($idItem, PDO::PARAM_INT),
            ':idChecklist' => array($idChecklist, PDO::PARAM_INT)
        ));
    }



}
