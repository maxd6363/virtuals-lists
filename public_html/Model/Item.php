<?php

namespace Model;


use DateTime;


class Item
{

    private $id;
    private $title;
    private $checked;
    private $creationDate;
    private $creator;
    private $tagged;
    private $tagText;
    private $tagColor;
    private $idChecklist;

    public function __construct(?int $id, string $title, bool $checked, DateTime $creationDate, ?User $creator, bool $tagged, ?string $tagText, ?string $tagColor, ?int $idChecklist)
    {
        $this->id = $id;
        $this->title = $title;
        $this->checked = $checked;
        $this->creationDate = $creationDate;
        $this->creator = $creator;
        $this->tagged = $tagged;
        $this->tagText = $tagText;
        $this->tagColor = $tagColor;
        $this->idChecklist = $idChecklist;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function isChecked(): bool
    {
        return $this->checked;
    }

    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function isTagged(): bool
    {
        return $this->tagged;
    }

    public function getTagText(): ?string
    {
        return $this->tagText;
    }

    public function getTagColor(): ?string
    {
        return $this->tagColor;
    }

    public function getIdChecklist(): ?int
    {
        return $this->idChecklist;
    }

}

?>
