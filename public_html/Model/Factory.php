<?php

namespace Model;

use Configuration\Utils;
use DateTime;
use Model\Checklist;
use Model\Item;
use Model\User;

class Factory
{
    public static function createChecklist($rowChecklist, $rowsItem, $rowsUser, $type): ?Checklist
    {
        if ($type == 'mysql') {
            $tabItem = array();
            foreach ($rowsItem as $row) {
                $keyUser = array_search($row['idCreator'], array_column($rowsUser, 'id'));

                $rowUser = $rowsUser[$keyUser];
                $tabItem[] = self::createItem($row, $rowUser, 'mysql');
            }

            return new Checklist($rowChecklist['id'], $rowChecklist['title'], $rowChecklist['idOwner'], null, $tabItem);
        }
        return null;
    }

    public static function createItem($row, $rowUser, $type): ?Item
    {
        if ($type == 'mysql') {
            if ($row['idCreator'] == null) {
                $user = null;
            } else {
                $user = self::createUser($rowUser, 'mysql');
            }

            return new Item($row['id'], $row['title'], $row['isChecked'], DateTime::createFromFormat("Y-m-d G:i:s", $row['creationDate']), $user, $row['isTagged'], $row['tagName'], $row['tagColor'], null);
        }
        return null;
    }


    public static function createUser($row, $type): ?User
    {
        if ($type == 'mysql') {
            if ($row == null) {
                return null;
            }
            $isAdmin = Utils::tinyIntToBool($row['isAdmin']);
            return new User($row['id'], $row['userName'], null, $row['userEmail'], $isAdmin, null);
        }
        return null;
    }
}
