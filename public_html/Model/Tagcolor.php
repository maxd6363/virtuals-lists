<?php
namespace Model;

class Tagcolor
{

    public const Primary = "primary";
    public const Secondary = "secondary";
    public const Success = "success";
    public const Danger = "danger";
    public const Warning = "warning";
    public const Info = "info";
    public const Dark = "dark";
    public const Muted = "muted";

    private function __construct() {}
}

?>