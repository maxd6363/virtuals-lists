<?php

namespace Model\Models;

use Controller\Validation;
use DateTime;
use Exception;
use Model\User;
use Model\Connection;
use Model\Factories\Factory as FactoriesFactory;
use Model\Gateway\UserGateway;
use Model\Gateway\ChecklistGateway;
use Model\Gateway\ItemGateway;
use Model\Factory;
use Model\Item;

class UserModel extends Model
{
    public function connectionUser($login, $password): ?User
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $login = Validation::sanitizeText($login);
        $password = Validation::sanitizeText($password);
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $userGateway = new UserGateway($connection);
        $passwordFromDB = $userGateway->getPassword($login);
        if (password_verify($password, $passwordFromDB)) {
            $isAdmin = $userGateway->isAdmin($login);
            $user = new User(null, $login, null, '', $isAdmin, null);
            $_SESSION['user'] = $user;
            return $user;
        }
        return null;
    }



    public function isActor(): ?User
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        return null;
    }

    public function disconnectUser()
    {
        session_unset();
        session_destroy();
        $_SESSION = array();
    }


    public function registerUser($username, $password, $email): bool
    {
        $username = Validation::sanitizeText($username);
        $password = Validation::sanitizeText($password);
        $email = Validation::sanitizeEmail($email);
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);

        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $userGateway = new UserGateway($connection);
        return $userGateway->registerUser($username, $hashedPassword, $email);
    }

    public function getPrivateChecklists($username): array
    {
        $tab = array();
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $checkListGateway = new ChecklistGateway($connection);
        $itemGateway = new ItemGateway($connection);
        $userGateway = new UserGateway($connection);

        $res = $checkListGateway->findPrivateChecklist($username);

        foreach ($res as $row) {
            $resItems = $itemGateway->findByIdChecklist($row['id']);
            $resUsers = $userGateway->findAll();

            $tab[] = Factory::createChecklist($row, $resItems, $resUsers, 'mysql');
        }

        return $tab;
    }

    public function addPrivateChecklist($name, $username)
    {

        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $checklistGateway = new ChecklistGateway($connection);
        $checklistGateway->addPrivateCheckList($name, $username);
    }


    public function addItemPrivate($user, $title, $tagText, $tagColor, $idCheckList)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $item = new Item(null, $title, false, new DateTime('now'), $user, true, $tagText, $tagColor, $idCheckList);
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $itemGateway->addPrivateItem($item, $user->getUserName());
    }


    public function deleteItemPrivate($idItem, $idCheckList)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $itemGateway->deletePrivateItem($idItem, $idCheckList);
    }

    public function checkItemPrivate($idItem, $idCheckList)
    {
        $this->changeCheckStatus(true, $idItem, $idCheckList);
    }

    public function uncheckItemPrivate($idItem, $idCheckList)
    {
        $this->changeCheckStatus(false, $idItem, $idCheckList);
    }



    private function changeCheckStatus($state, $idItem, $idCheckList)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $itemGateway->updateIsChecked($state, $idItem, $idCheckList);
    }


    public function deletePrivateChecklist($id)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $checklistGateway = new ChecklistGateway($connection);
        $tabIdItem = $checklistGateway->findAllItem($id);
        foreach ($tabIdItem as $idItem) {
            $itemGateway->delete($idItem['id']);
        }
        $checklistGateway->delete($id);
    }
}
