<?php

namespace Model\Models;

use DateTime;
use Model\User;
use Model\Connection;
use Model\Factory;
use Model\Gateway\ChecklistGateway;
use Model\Gateway\ItemGateway;
use Model\Gateway\UserGateway;
use Model\Item;

class GuestModel extends Model

{
    public function getPublicChecklists(): array
    {
        $tab = array();
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $checkListGateway = new ChecklistGateway($connection);
        $itemGateway = new ItemGateway($connection);
        $userGateway = new UserGateway($connection);

        $res = $checkListGateway->findPublicChecklist();
        
        foreach ($res as $row) {
            $resItems = $itemGateway->findByIdChecklist($row['id']);
            $resUsers = $userGateway->findAll();

            $tab[] = Factory::createChecklist($row, $resItems, $resUsers, 'mysql');
        }

        return $tab;
    }

    public function isActor(): ?User
    {
        return null;
    }

    public function getNbChecklists(): int
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $checklistGateway = new ChecklistGateway($connection);
        return $checklistGateway->getNbChecklists();
    }

    public function getNbUsers(): int
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $userGateway = new UserGateway($connection);



        return $userGateway->getNbUsers();
    }


    public function addPublicChecklist($name)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $checklistGateway = new ChecklistGateway($connection);
        $checklistGateway->addPublicCheckList($name);
    }


    public function addItemPublic($title, $tagText, $tagColor, $idCheckList)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $item = new Item(null, $title, false, new DateTime('now'), null, true, $tagText, $tagColor, $idCheckList);
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $itemGateway->addPublicItem($item);
    }

    public function deleteItemPublic($idItem, $idCheckList)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $itemGateway->deletePublicItem($idItem, $idCheckList);
    }


    public function checkItemPublic($idItem, $idCheckList)
    {
        $this->changeCheckStatus(true, $idItem, $idCheckList);
    }

    public function uncheckItemPublic($idItem, $idCheckList)
    {
        $this->changeCheckStatus(false, $idItem, $idCheckList);
    }



    private function changeCheckStatus($state, $idItem, $idCheckList)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $itemGateway->updateIsChecked($state, $idItem, $idCheckList);
    }


    public function deletePublicChecklist($id)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $checklistGateway = new ChecklistGateway($connection);
        $tabIdItem = $checklistGateway->findAllItem($id);
        foreach ($tabIdItem as $idItem) {
            $itemGateway->deletePublicItem($idItem['id'], $id);
        }
        $checklistGateway->delete($id);
    }
}
