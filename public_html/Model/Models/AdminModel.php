<?php

namespace Model\Models;

use DateTime;
use Model\User;
use Model\Connection;
use Model\Factory;
use Model\Gateway\ChecklistGateway;
use Model\Gateway\ItemGateway;
use Model\Gateway\UserGateway;
use Model\Item;

class AdminModel extends Model

{
    public function getAllPrivateChecklists(): array
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $tab = array();
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $checkListGateway = new ChecklistGateway($connection);
        $itemGateway = new ItemGateway($connection);
        $userGateway = new UserGateway($connection);

        $res = $checkListGateway->findAllPrivateChecklist();
        
        foreach ($res as $row) {
            $resItems = $itemGateway->findByIdChecklist($row['id']);
            $resUsers = $userGateway->findAll();
            $tab[] = Factory::createChecklist($row, $resItems, $resUsers, 'mysql');
        }
        return $tab;
    }

    public function isActor(): ?User
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        return null;
    }


    public function deleteItemPrivate($idItem, $idCheckList)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $itemGateway->deletePrivateItemAdmin($idItem, $idCheckList);
    }


    public function getTabUsername()
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $tabUsername = array();
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $userGateway = new UserGateway($connection);
        $tab = $userGateway->findAllUsername();
        foreach ($tab as $row) {
            $tabUsername[$row['id']] = $row['userName'];
        }
        return $tabUsername;
    }


    public function deletePrivateChecklist($id)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $checklistGateway = new ChecklistGateway($connection);
        $tabIdItem = $checklistGateway->findAllItem($id);
        foreach ($tabIdItem as $idItem) {
            $itemGateway->delete($idItem['id']);
        }
        $checklistGateway->delete($id);
    }


    public function getAllUsers(): array
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $tab = array();
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $userGateway = new UserGateway($connection);


        $res = $userGateway->findAll();
        foreach ($res as $row) {
            $tab[] = Factory::createUser($row, 'mysql');
        }
        return $tab;
    }

    public function findAllInfoUsers(): array
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $tab = array();
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $userGateway = new UserGateway($connection);

        $tabChecklist = $userGateway->findChecklistInfoUsers();
        $tabItem = $userGateway->findItemInfoUsers();

        $tabInfo = array();

        for ($i = 0; $i < count($tabChecklist); $i++) {
            $tabInfo[] = array($tabChecklist[$i]['nbChecklist'], $tabItem[$i]['nbItem']);
        }
        return $tabInfo;
    }

    public function deleteUser($idUser)
    {
        global $databaseName, $databaseLogin, $databasePassword;
        $connection = new Connection($databaseName, $databaseLogin, $databasePassword);
        $itemGateway = new ItemGateway($connection);
        $checklistGateway = new ChecklistGateway($connection);
        $userGateway = new UserGateway($connection);

        $tabIdChecklists = $userGateway->findAllIdChecklistsForUser($idUser);
        foreach($tabIdChecklists as $idCheckList){

            $tabIdItem = $checklistGateway->findAllItem($idCheckList['id']);
            foreach ($tabIdItem as $idItem) {
                $itemGateway->delete($idItem['id']);
            }
            $checklistGateway->delete($idCheckList['id']);
        }
        $userGateway->delete($idUser);


    }

    
}
