<?php

namespace Controller;

use DateTime;
use PDOException;
use Exception;
use Model\Item;
use Model\Models\GuestModel;
use Model\Models\UserModel;

class GuestController
{

    function __construct(?string $action)
    {
        global $dir, $view; // Nécéssaire pour les utiliser en tant que variables globales.
        $errors = array(); // Initialisation du tableau d'erreurs.
        try {
            switch ($action) {
                case NULL: // Si aucune action, on réinitialise le premier appel.
                case "accueil":
                    $this->accueil();
                    break;
                case "checklistsPubliques":
                    $this->showPublicsChecklists();
                    break;
                case "signIn":
                    $this->showAuthentification();
                    break;
                case "signInValidate":
                    $this->authentificate();
                    break;
                case "signUp":
                    $this->showEnregistrement();
                    break;
                case "signUpValidate":
                    $this->enregistrer();
                    break;
                case "addPublicChecklist":
                    $this->addPublicChecklist();
                    break;
                case "addPublicItem":
                    $this->addPublicItem();
                    break;
                case "deletePublicItem":
                    $this->deletePublicItem();
                    break;
                case "uncheckPublicItem":
                    $this->uncheckPublicItem();
                    break;
                case "checkPublicItem":
                    $this->checkPublicItem();
                    break;
                case "deletePublicChecklist":
                    $this->deletePublicChecklist();
                    break;



                default: // Si l'action côté client est incorrecte.
                    $errors[] = "Erreur d'appel php : mauvaise action (récupérée par le Controleur).";
                    require($dir . $view["erreur_client"]);
                    break;
            }
        } catch (PDOException $e1) { // Erreur dans la base de données.
            $errors[] = $e1->getTraceAsString();
            require($dir . $view["erreur_sql"]);
        } catch (Exception $e2) { // Autres erreurs côté serveur.
            $errors[] = $e2->getTraceAsString();
            require($dir . $view["erreur_serveur"]);
        }
        exit(0);
    }

    function accueil()
    {
        global $dir, $view;
        $model = new GuestModel();
        $nbChecklists = $model->getNbChecklists();
        $nbUsers = $model->getNbUsers();
        if (isset($_SESSION['user'])) {
            $user = Validation::sanitizeUser($_SESSION['user']);
        }
        require($dir . $view["accueil"]);
    }


    function showPublicsChecklists()
    {
        global $dir, $view;
        $model = new GuestModel();
        $checklists = $model->getPublicChecklists();
        if (isset($_SESSION['user'])) {
            $user = Validation::sanitizeUser($_SESSION['user']);
        }
        require($dir . $view["checklistsPubliques"]);
    }

    function showAuthentification()
    {
        global $dir, $view;
        require($dir . $view["authentification"]);
    }

    function authentificate()
    {
        global $dir, $view;
        $username = $_POST['identifier'];
        $password = $_POST['password'];
        $username = Validation::sanitizeText($username);
        $password = Validation::sanitizeText($password);
        $modelUser = new UserModel();
        $userConnected = $modelUser->connectionUser($username, $password);
        $connected = $userConnected != null;
        require($dir . $view['authentification']);

        if ($userConnected != null) {
            // header( "refresh:3;url=". $dir . $view['accueil']);
            // ne marche pas (illegal dans la config actuelle)
        }
    }

    function showEnregistrement()
    {
        global $dir, $view;
        require($dir . $view["enregistrement"]);
    }



    function enregistrer()
    {

        global $dir, $view;
        $username = $_POST['identifier'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $username = Validation::sanitizeText($username);
        $email = Validation::sanitizeEmail($email);
        $password = Validation::sanitizeText($password);
        $modelUser = new UserModel();
        //force la deconnection de l'utilisateur
        $modelUser->disconnectUser();
        $userRegistered = $modelUser->registerUser($username, $password, $email);
        require($dir . $view["enregistrement"]);


        if ($userRegistered == true) {
            // header( "refresh:3;url=". $dir . $view['accueil']);
            // ne marche pas (illegal dans la config actuelle)
        }
    }




    function addPublicChecklist()
    {
        if (!isset($_POST['name'])) {
            throw new Exception("No name for checklist");
        }
        $name = Validation::sanitizeText($_POST['name']);
        $model = new GuestModel();
        $model->addPublicChecklist($name);
        $this->showPublicsChecklists();
    }

    function addPublicItem()
    {
        global $tagColors;
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
        }
        if (!isset($_POST['title'])) {
            throw new Exception("Item title not found");
        }
        if (!isset($_POST['tagText'])) {
            throw new Exception("Tag text not found");
        }
        if (!isset($_POST['tagColor'])) {
            throw new Exception("Tag color not found");
        }
        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $title = Validation::sanitizeText($_POST['title']);
        $tagText = Validation::sanitizeText($_POST['tagText']);
        $tagColor = Validation::sanitizeText($tagColors[$_POST['tagColor']]);

        $guestModel = new GuestModel();
        $guestModel->addItemPublic($title, $tagText, $tagColor, $idCheckList);
        $this->showPublicsChecklists();
    }

    function deletePublicItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
            // pour être sur que l'item fait bien parti de sa checklist (sécurité)
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $guestModel = new GuestModel();
        $guestModel->deleteItemPublic($idItem, $idCheckList);
        $this->showPublicsChecklists();
    }


    function uncheckPublicItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
            // pour être sur que l'item fait bien parti de sa checklist (sécurité)
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $guestModel = new GuestModel();
        $guestModel->uncheckItemPublic($idItem, $idCheckList);
        $this->showPublicsChecklists();
    }


    function checkPublicItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
            // pour être sur que l'item fait bien parti de sa checklist (sécurité)
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $guestModel = new GuestModel();
        $guestModel->checkItemPublic($idItem, $idCheckList);
        $this->showPublicsChecklists();
    }


    function deletePublicChecklist()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("No id for checklist");
        }
        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $model = new GuestModel();
        $model->deletePublicChecklist($idCheckList);
        $this->showPublicsChecklists();
    }
}
