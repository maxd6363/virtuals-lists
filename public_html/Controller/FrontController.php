<?php

namespace Controller;

use Exception;
use Model\Models\GuestModel;

class FrontController
{
    private $listActions;

    function __construct()
    {
        global $dir, $view; // Nécéssaire pour les utiliser en tant que variables globales.
        $errors = array(); // Initialisation du tableau d'erreurs.

        $this->listActions = array(
            "Admin" => array("allChecklistsPrivees", "addPrivateItemAdmin","deletePrivateItemAdmin","uncheckPrivateItemAdmin","checkPrivateItemAdmin","deletePrivateChecklistAdmin","allUsers","deleteUser"), // Actions possibles pour un administrateur.
            "User" => array("signOut", "checklistsPrivees","addPrivateChecklist","addPrivateItem","deletePrivateItem","uncheckPrivateItem","checkPrivateItem","deletePrivateChecklist"), // Actions possibles pour un utilisateur.
        );
        if (isset($_REQUEST["action"])) {
            $action = $_REQUEST["action"];
        } else {
            $action = null;
        }

        $stringActor = $this->isGoodAction($action);
        $classModel = "Model\Models\\" . $stringActor . "Model";

        try {
            $actorModel = new $classModel();
            $actor = $actorModel->isActor();
            if($stringActor != "Guest") {
                if($action == null) {
                    require($dir . $view["authentification"]);
                } else {
                    $controllerClass = "Controller\\" . $stringActor . "Controller";
                    new $controllerClass($action);
                }
            } else {
                new GuestController($action);
            }
            
        } catch (Exception $e) {
            $errors[] = $e->getTraceAsString();
            require($dir . $view['erreur_client']);
        }
    }

    function isGoodAction($action): string
    {
        foreach (array_keys($this->listActions) as $role) {
            if (in_array($action, $this->listActions[$role])) {
                return $role;
            }
        }
        return "Guest";
    }

}
