<?php

namespace Controller;

use PDOException;
use Exception;
use Model\Models\UserModel;
use Model\Models\GuestModel;

class UserController
{

    function __construct(?string $action)
    {
        global $dir, $view; // Nécéssaire pour les utiliser en tant que variables globales.
        $errors = array(); // Initialisation du tableau d'erreurs.
        try {
            switch ($action) {
                case NULL: // Si aucune action, on réinitialise le premier appel.
                case "accueil":
                    $this->accueil();
                    break;
                case "signOut":
                    $this->disconnectUser();
                    break;
                case "checklistsPrivees":
                    $this->showPrivateChecklists();
                    break;
                case "addPrivateChecklist":
                    $this->addPrivateChecklist();
                    break;
                case "addPrivateItem":
                    $this->addPrivateItem();
                    break;
                case "deletePrivateItem":
                    $this->deletePrivateItem();
                    break;
                case "uncheckPrivateItem":
                    $this->uncheckPrivateItem();
                    break;
                case "checkPrivateItem":
                    $this->checkPrivateItem();
                    break;
                case "deletePrivateChecklist":
                    $this->deletePrivateChecklist();
                    break;



                default: // Si l'action côté client est incorrecte.
                    $errors[] = "Erreur d'appel php : mauvaise action (récupérée par le Controleur).";
                    require($dir . $view['erreur_client']);
                    break;
            }
        } catch (PDOException $e1) { // Erreur dans la base de données.
            $errors[] = $e1->getTraceAsString();
            require($dir . $view['erreur_sql']);
        } catch (Exception $e2) { // Autres erreurs côté serveur.
            $errors[] = $e2->getTraceAsString();
            require($dir . $view['erreur_serveur']);
        }
        exit(0);
    }

    function accueil()
    {
        global $dir, $view;
        $model = new GuestModel();
        $nbChecklists = $model->getNbChecklists();
        $nbUsers = $model->getNbUsers();
        if (isset($_SESSION['user'])) {
            $user = Validation::sanitizeUser($_SESSION['user']);
        }
        require($dir . $view["accueil"]);
    }


    function disconnectUser()
    {
        global $dir, $view;
        $model = new UserModel();
        $checklists = $model->disconnectUser();
        require($dir . $view["accueil"]);
    }

    function showPrivateChecklists()
    {
        global $dir, $view;
        $model = new UserModel();

        if (isset($_SESSION['user'])) {
            if ($_SESSION['user'] != null) {
                $username = Validation::sanitizeText($_SESSION['user']->getUsername());
                $checklists = $model->getPrivateChecklists($username);
                $user = Validation::sanitizeUser($_SESSION['user']);
            }
        }

        require($dir . $view["checklistsPrivees"]);
    }


    function addPrivateChecklist()
    {
        if (!isset($_POST['name'])) {
            throw new Exception("No name for checklist");
        }
        if (!isset($_SESSION['user'])) {
            throw new Exception("User must be connected");
        }
        $username = Validation::sanitizeText($_SESSION['user']->getUserName());
        $name = Validation::sanitizeText($_POST['name']);
        $model = new UserModel();
        $model->addPrivateChecklist($name, $username);
        $this->showPrivateChecklists();
    }

    function addPrivateItem()
    {
        global $tagColors;
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
        }
        if (!isset($_POST['title'])) {
            throw new Exception("Item title not found");
        }
        if (!isset($_POST['tagText'])) {
            throw new Exception("Tag text not found");
        }
        if (!isset($_POST['tagColor'])) {
            throw new Exception("Tag color not found");
        }
        if (!isset($_SESSION['user'])) {
            throw new Exception("User must be connected");
        }
        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $title = Validation::sanitizeText($_POST['title']);
        $tagText = Validation::sanitizeText($_POST['tagText']);
        $tagColor = Validation::sanitizeText($tagColors[$_POST['tagColor']]);
        $user = $_SESSION['user'];

        $userModel = new UserModel();
        $userModel->addItemPrivate($user, $title, $tagText, $tagColor, $idCheckList);
        $this->showPrivateChecklists();
    }

    function deletePrivateItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $userModel = new UserModel();
        $userModel->deleteItemPrivate($idItem, $idCheckList);
        $this->showPrivateChecklists();
    }

    function uncheckPrivateItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
            // pour être sur que l'item fait bien parti de sa checklist (sécurité)
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $userModel = new UserModel();
        $userModel->uncheckItemPrivate($idItem, $idCheckList);
        $this->showPrivateChecklists();
    }


    function checkPrivateItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
            // pour être sur que l'item fait bien parti de sa checklist (sécurité)
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $userModel = new UserModel();
        $userModel->checkItemPrivate($idItem, $idCheckList);
        $this->showPrivateChecklists();
    }

    function deletePrivateChecklist()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("No id for checklist");
        }
        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $model = new UserModel();
        $model->deletePrivateChecklist($idCheckList);
        $this->showPrivateChecklists();
    }



}
