<?php

namespace Controller;

use Exception;
use Model\User;

class Validation
{

    static function validateAction($action)
    {
        if (!isset($action)) {
            throw new Exception("Aucune action choisie.");
        }
    }


    static function sanitizeEmail($email): string
    {
        $sanitized_email = filter_var($email, FILTER_SANITIZE_EMAIL);
        if (filter_var($sanitized_email, FILTER_VALIDATE_EMAIL)) {
            return $sanitized_email;
        }
        throw new Exception("Email invalide.");
    }


    static function sanitizeText($text): string
    {
        return filter_var($text, FILTER_SANITIZE_STRING);
    }


    static function sanitizeNumber($number): int
    {
        return filter_var($number, FILTER_SANITIZE_NUMBER_INT);
    }

    static function sanitizeUser(User $user): User
    {
        return new User(
            $user->getId(),
            filter_var($user->getUserName(), FILTER_SANITIZE_STRING),
            null,
            filter_var($user->getUserEmail(), FILTER_SANITIZE_EMAIL),
            $user->isAdmin(),
            $user->getChecklist()
        );
    }
}
