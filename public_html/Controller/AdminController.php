<?php

namespace Controller;

use PDOException;
use Exception;
use Model\Models\AdminModel;
use Model\Models\GuestModel;
use Model\Models\UserModel;

class AdminController
{

    function __construct(?string $action)
    {
        global $dir, $view; // Nécéssaire pour les utiliser en tant que variables globales.
        $errors = array(); // Initialisation du tableau d'erreurs.
        try {

            switch ($action) {
                case NULL: // Si aucune action, on réinitialise le premier appel.
                case "accueil":
                    $this->accueil();
                    break;
                case "allChecklistsPrivees":
                    $this->showAllPrivateChecklists();
                    break;
                case "addPrivateItemAdmin":
                    $this->addPrivateItem();
                    break;
                case "deletePrivateItemAdmin":
                    $this->deletePrivateItem();
                    break;
                case "uncheckPrivateItemAdmin":
                    $this->uncheckPrivateItem();
                    break;
                case "checkPrivateItemAdmin":
                    $this->checkPrivateItem();
                    break;
                case "deletePrivateChecklistAdmin":
                    $this->deletePrivateChecklist();
                    break;
                case "allUsers":
                    $this->showAllUsers();
                    break;
                case "deleteUser":
                    $this->deleteUser();
                    break;






                default: // Si l'action côté client est incorrecte.
                    $errors[] = "Erreur d'appel php : mauvaise action (récupérée par le Controleur).";
                    require($dir . $view['erreur_client']);
                    break;
            }
        } catch (PDOException $e1) { // Erreur dans la base de données.
            $errors[] = $e1->getTraceAsString();
            require($dir . $view['erreur_sql']);
        } catch (Exception $e2) { // Autres erreurs côté serveur.
            $errors[] = $e2->getTraceAsString();
            require($dir . $view['erreur_serveur']);
        }
        exit(0);
    }

    function accueil()
    {
        global $dir, $view;
        $model = new GuestModel();
        $nbChecklists = $model->getNbChecklists();
        $nbUsers = $model->getNbUsers();
        if (isset($_SESSION['user'])) {
            $user = Validation::sanitizeUser($_SESSION['user']);
        }
        require($dir . $view['accueil']);
    }

    function showAllPrivateChecklists()
    {
        global $dir, $view;
        $adminModel = new AdminModel();

        if (isset($_SESSION['user'])) {
            if ($_SESSION['user'] != null) {
                $user = Validation::sanitizeUser($_SESSION['user']);
                if ($user->isAdmin()) {
                    $checklists = $adminModel->getAllPrivateChecklists();
                    $tabUsername = $adminModel->getTabUsername();
                    $user = Validation::sanitizeUser($_SESSION['user']);
                }
            }
        }

        require($dir . $view['toutesChecklistsPrivees']);
    }



    function addPrivateItem()
    {
        global $tagColors;
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
        }
        if (!isset($_POST['title'])) {
            throw new Exception("Item title not found");
        }
        if (!isset($_POST['tagText'])) {
            throw new Exception("Tag text not found");
        }
        if (!isset($_POST['tagColor'])) {
            throw new Exception("Tag color not found");
        }
        if (!isset($_SESSION['user'])) {
            throw new Exception("User must be connected");
        }
        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $title = Validation::sanitizeText($_POST['title']);
        $tagText = Validation::sanitizeText($_POST['tagText']);
        $tagColor = Validation::sanitizeText($tagColors[$_POST['tagColor']]);
        $user = Validation::sanitizeUser($_SESSION['user']);

        $userModel = new UserModel();
        $userModel->addItemPrivate($user, $title, $tagText, $tagColor, $idCheckList);
        $this->showAllPrivateChecklists();
    }

    function deletePrivateItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $adminModel = new AdminModel();
        $adminModel->deleteItemPrivate($idItem, $idCheckList);
        $this->showAllPrivateChecklists();
    }

    function uncheckPrivateItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
            // pour être sur que l'item fait bien parti de sa checklist (sécurité)
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $userModel = new UserModel();
        $userModel->uncheckItemPrivate($idItem, $idCheckList);
        $this->showAllPrivateChecklists();
    }


    function checkPrivateItem()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("Checklist not found");
            // pour être sur que l'item fait bien parti de sa checklist (sécurité)
        }
        if (!isset($_POST['id'])) {
            throw new Exception("Item id not found");
        }

        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $idItem = Validation::sanitizeText($_POST['id']);

        $userModel = new UserModel();
        $userModel->checkItemPrivate($idItem, $idCheckList);
        $this->showAllPrivateChecklists();
    }


    function deletePrivateChecklist()
    {
        if (!isset($_POST['idChecklist'])) {
            throw new Exception("No id for checklist");
        }
        $idCheckList = Validation::sanitizeNumber($_POST['idChecklist']);
        $model = new AdminModel();
        $model->deletePrivateChecklist($idCheckList);
        $this->showAllPrivateChecklists();
    }

    function showAllUsers()
    {
        global $dir, $view;
        $adminModel = new AdminModel();

        if (isset($_SESSION['user'])) {
            if ($_SESSION['user'] != null) {
                $user = Validation::sanitizeUser($_SESSION['user']);
                if ($user->isAdmin()) {
                    $users = $adminModel->getAllUsers();
                    $infosUsers = $adminModel->findAllInfoUsers();
                }
            }
        }
        require($dir . $view['allUsers']);
    }

    function deleteUser()
    {
        $adminModel = new AdminModel();

        if (isset($_SESSION['user'])) {
            if ($_SESSION['user'] != null) {
                $user = Validation::sanitizeUser($_SESSION['user']);
                if ($user->isAdmin()) {
                    $idUser = Validation::sanitizeNumber($_POST['idUser']);
                    $adminModel->deleteUser($idUser);
                    $user = Validation::sanitizeUser($_SESSION['user']);
                }
            }
        }
        $this->showAllUsers();
    }
}
