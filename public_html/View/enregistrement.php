<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Gaspard ANDRIEU et Maxime POULAIN">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="Ressources/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Particles.js -->
    <link rel="stylesheet" media="screen" href="Ressources/particles/css/style.css">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="Ressources/fontawesome/css/all.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="Ressources/custom-styles.css">

    <!-- Title -->
    <title>Virtual Lists | Enregistrement</title>

    <!-- Icon -->
    <link rel="icon" href="Ressources/images/logo.png" />
</head>

<body class="bodyFade" style="overflow: auto">
    <div id="particles-js" class="position-fixed"></div>
    <div class="sticky-top pb-3">
        <div class="container-fluid d-flex justify-content-center">
            <div class="card mt-3">
                <div class="card-header-tab card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal d-flex justify-content-center">
                        <form method="POST" class="align-self-start mr-2 mf-0">
                            <button class="hidden btn btn-dark icon-circle-arrow-left font-weight-normal" type="submit" name="action" value="accueil">&lt;</button>
                        </form>

                        <h1 class="h2 font-weight-normal m-0 p-0">Connexion</h1>
                    </div>
                </div>
                <div class="card-body my-3 mx-3 d-flex justify-content-center flex-column">
                    <form class="form-signin" method="POST">
                        <input type="text" name="identifier" class="form-control mb-3" placeholder="Identifiant" required autofocus>
                        <input type="email" name="email" class="form-control mb-3" placeholder="Email" required>
                        <input type="password" name="password" class="form-control mb-3" placeholder="Mot de passe" required>
                        <button class="btn btn-lg btn-primary btn-block" type="submit" name="action" value="signUpValidate">S'enregistrer</button>
                    </form>
                    <?php
                    if (isset($userRegistered)) {
                        if ($userRegistered == true) {
                            echo '<div class="mt-4 alert alert-success">Bienvenue sur notre site !</div>';
                            echo '<form method="POST">
							            <button class="btn btn-lg btn-block my-2 text-muted" type="submit" name="action" value="signIn">Se connecter</button>
						            </form>';
                        } else {
                            echo '<div class="mt-4 alert alert-danger">Impossible de s\'enregistrer...</div>';
                        }
                    }
                    ?>
                    <div class="d-flex justify-content-center flex-column">
                        <hr class="my-1">
                        <p class="mt-2 mb-3 text-muted align-self-center">&copy; Virtual Drops - 2019</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="Ressources/bootstrap/js/bootstrap.min.js"></script>
    <script src="Ressources/particles/particles.js"></script>
    <script src="Ressources/particles/js/app.js"></script>
    <script src="Ressources/sticky.js"></script>

    <script>
        $(document).ready(function() {
            $("").sticky({
                topSpacing: 0
            });
        });
    </script>
</body>

</html>