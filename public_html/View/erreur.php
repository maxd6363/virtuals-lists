<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Gaspard ANDRIEU et Maxime POULAIN">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="Ressources/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Particles.js -->
    <link rel="stylesheet" media="screen" href="Ressources/particles/css/style.css">

    <!-- Title -->
    <title>Virtual Lists | Erreur</title>

    <!-- Icon -->
    <link rel="icon" href="Ressources/images/logo.png" />
</head>

<body>


    <div id="particles-js"></div>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <!-- Brand -->
        <a class="navbar-brand" href="#">Virtual Lists</a>

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav flex-grow-1">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Accès rapide<div class="d-inline"></div> <span class="badge badge-danger">Nouveau</span> </a>
                    <div class="dropdown-menu dropdown-info" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item waves-effect waves-light" href="#">A faire</a>
                        <a class="dropdown-item waves-effect waves-light" href="#">Fait</a>
                        <a class="dropdown-item waves-effect waves-light" href="#">Gérer mes checklists</a>
                    </div>
                </li>
            </ul>
            <div>
                <a href="View/login.php">
                    <button type="button" class="btn btn-success">Se connecter</button>
                </a>
                <a href="View/register.php">
                    <button type="button" class="btn btn-danger">S'inscrire</button>
                </a>

            </div>
        </div>
    </nav>






    <?php
    if (isset($dVueEreur)) {
        foreach ($dVueEreur as $value) {
            echo '<div class="mt-3"></div>';
            echo '<div class="alert alert-danger h1 mr-3 ml-3" role="alert">' . $value . '</div>';
        }
    }
    ?>


    <!-- scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="Ressources/bootstrap/js/bootstrap.min.js"></script>
    <script src="Ressources/particles/particles.js"></script>
    <script src="Ressources/particles/js/app.js"></script>

</body>

</html>