<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Gaspard ANDRIEU et Maxime POULAIN">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="Ressources/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Particles.js -->
    <link rel="stylesheet" media="screen" href="Ressources/particles/css/style.css">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="Ressources/fontawesome/css/all.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="Ressources/custom-styles.css">

    <!-- Title -->
    <title>Virtual Lists | Erreur 500</title>

    <!-- Icon -->
    <link rel="icon" href="Ressources/images/logo.png" />
</head>

<body style="overflow: auto">
    <div id="particles-js" class="position-fixed"></div>
    <div class="sticky-top pb-3">
        <div class="container">
            <canvas class="matrix-canvas position-fixed vw-100 vh-100" id="matrix-canvas-1">Le canvas n'est pas supporté par votre navigateur.</canvas>
            <canvas class="matrix-canvas position-fixed vw-100 vh-100" id="matrix-canvas-2">Le canvas n'est pas supporté par votre navigateur.</canvas>
            <div class="card mt-3">
                <div class="card-body">
                    <div class="text-center">
                        <div class="error mx-auto" data-text="500">500</div>
                        <p class="lead text-gray-800 mb-5"><i class="fas fa-server"></i> Une erreur côté serveur est survenue. <i class="fas fa-server"></i></p>
                        <?php
                        echo '<p class="text-gray-500 mb-0">Informations sur ' . "l'erreur" . ' <i class="fas fa-arrow-circle-right"></i> ';
                        if (!isset($erros)) {
                            echo "Aucune information disponible (accès à la page illégal).";
                        } else {
                            $isFirst = true;
                            foreach ($errors as $row) {
                                if ($isFirst) {
                                    $isFirst = false;
                                } else {
                                    echo ' <i class="fas fa-plus"></i> ';
                                }
                                echo $row;
                            }
                            if (count($errors) == 0) {
                                echo "Aucune information disponible (accès à la page illégal).";
                            }
                            echo '</p>';
                        }
                        ?>
                        <br /><br />
                        <p class="text-gray-500 mb-0">La matrice est cassée... Mais ce n'est pas de votre faute alors pas de panique !</p>
                        <p class="text-gray-500 mb-0">Vous pouvez encore inverser le processus en cliquant juste en dessous !</p>
                        <br />
                        <form method="post">
                            <input type="hidden" name="action" value="accueil"></input>
                            <input type="submit" value="&larr; Retourner à l'accueil" class="bg-transparent border-0 text-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="Ressources/bootstrap/js/bootstrap.min.js"></script>
    <script src="Ressources/particles/particles.js"></script>
    <script src="Ressources/particles/js/app.js"></script>
    <script src="Ressources/sticky.js"></script>
    <script src="Ressources/custom-scripts.js"></script>
    <script>
        $(document).ready(function() {
            $("").sticky({
                topSpacing: 0
            });
        });
    </script>

</body>

</html>