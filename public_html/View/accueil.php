<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Gaspard ANDRIEU et Maxime POULAIN">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="Ressources/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Particles.js -->
    <link rel="stylesheet" media="screen" href="Ressources/particles/css/style.css">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="Ressources/fontawesome/css/all.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="Ressources/custom-styles.css">

    <!-- Title -->
    <title>Virtual Lists | Accueil</title>

    <!-- Icon -->
    <link rel="icon" href="Ressources/images/logo.png" />
</head>

<body class="bodyFade" style="overflow: auto">
    <div id="particles-js" class="position-fixed"></div>
    <div class="sticky-top pb-3">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <!-- Brand -->
            <a class="navbar-brand" href="">Virtual Lists</a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav flex-grow-1">
                    <li class="nav-item">
                        <form method="post">
                            <input type="hidden" name="action" value="accueil"></input>
                            <input type="submit" value="Accueil" class="bg-transparent border-0 nav-link active">
                        </form>
                    </li>
                    <li class="nav-item">
                        <form method="post">
                            <input type="hidden" name="action" value="checklistsPubliques"></input>
                            <input type="submit" value="Checklists publiques" class="bg-transparent border-0 nav-link">
                        </form>
                    </li>
                    <?php
                    if (isset($user)) {


                        if ($user != null) { // L'utilisateur est connecté.
                            echo '<li class="nav-item">
                            <form method="post">
                                <input type="hidden" name="action" value="checklistsPrivees"></input>
                                <input type="submit" value="Vos checklists" class="bg-transparent border-0 nav-link">
                            </form>
                        </li>';
                            if ($user->isAdmin()) { // L'utilisateur est également un administrateur.
                                echo '<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Accès global<div class="d-inline"></div> <span class="badge badge-danger">Administration</span> </a>
                                <div class="dropdown-menu dropdown-info" aria-labelledby="navbarDropdownMenuLink">
                                    <form method="post">
                                        <input type="hidden" name="action" value="allChecklistsPrivees"></input>
                                        <input type="submit" value="Toutes les checklists privées" class="dropdown-item waves-effect waves-light">
                                    </form>
                                    <form method="post">
                                        <input type="hidden" name="action" value="allUsers"></input>
                                        <input type="submit" value="Tous les utilisateurs" class="dropdown-item waves-effect waves-light">
                                    </form>
                                </div>


                                </li>';
                            }
                        }
                    }
                    ?>
                </ul>
                <div>

                    <form method="post" class="d-flex">
                        <?php
                        if (!isset($user)) {
                            $user = null;
                        }
                        if ($user == null) {
                            echo '<input type="hidden" name="action" value="signIn"></input>
                                <input type="submit" value="Se connecter / S\'inscrire" class="nav-link btn btn-primary"></input>';
                        } else {
                            echo '<input type="hidden" name="action" value="signOut"></input>
                                <input type="submit" value="Se déconnecter (' . $user->getUserName() . ')" class="nav-link btn btn-primary"></input>';
                        }
                        ?>
                    </form>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="card mt-3 bg-transparent border-white">
                <div class="card-body">
                    <div class="jumbotron bg-transparent text-white">
                        <h1 class="display-4">Virtual Lists</h1>
                        <hr class="my-4">
                        <p class="lead">Virtual Lists est un site de To Do Lists développé par l'équipe Virtual Drops&copy; (composée pour ce projet de Gaspard ANDRIEU et Maxime POULAIN).</p>
                        <hr class="my-4">
                        <?php
                        if (!isset($nbUsers))
                            $nbUsers = 0;

                        if (!isset($nbChecklists))
                            $nbChecklists = 0;

                        echo "<p>Nombre d'utilisateurs inscrits : $nbUsers</p>";
                        echo "<p>Nombre de checklists faites : $nbChecklists</p>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="Ressources/bootstrap/js/bootstrap.min.js"></script>
    <script src="Ressources/particles/particles.js"></script>
    <script src="Ressources/particles/js/app.js"></script>
    <script src="Ressources/sticky.js"></script>
    <script>
        $(document).ready(function() {
            $("").sticky({
                topSpacing: 0
            });
        });
    </script>

</body>

</html>