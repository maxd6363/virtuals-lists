<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Gaspard ANDRIEU et Maxime POULAIN">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="Ressources/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Particles.js -->
    <link rel="stylesheet" media="screen" href="Ressources/particles/css/style.css">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="Ressources/fontawesome/css/all.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="Ressources/custom-styles.css">

    <!-- Title -->
    <title>Virtual Lists | Checklists privées</title>

    <!-- Icon -->
    <link rel="icon" href="Ressources/images/logo.png" />
</head>

<body class="bodyFade" style="overflow: auto">
    <div id="particles-js" class="position-fixed"></div>
    <div class="sticky-top pb-3">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <!-- Brand -->
            <a class="navbar-brand" href="">Virtual Lists</a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav flex-grow-1">
                    <li class="nav-item">
                        <form method="post">
                            <input type="hidden" name="action" value="accueil"></input>
                            <input type="submit" value="Accueil" class="bg-transparent border-0 nav-link">
                        </form>
                    </li>
                    <li class="nav-item">
                        <form method="post">
                            <input type="hidden" name="action" value="checklistsPubliques"></input>
                            <input type="submit" value="Checklists publiques" class="bg-transparent border-0 nav-link">
                        </form>
                    </li>
                    <?php
                    if (isset($user)) {


                        if ($user != null) { // L'utilisateur est connecté.
                            echo '<li class="nav-item">
                            <form method="post">
                                <input type="hidden" name="action" value="checklistsPrivees"></input>
                                <input type="submit" value="Vos checklists" class="bg-transparent border-0 nav-link active">
                            </form>
                        </li>';
                            if ($user->isAdmin()) { // L'utilisateur est également un administrateur.
                                echo '<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Accès global<div class="d-inline"></div> <span class="badge badge-danger">Administration</span> </a>
                                <div class="dropdown-menu dropdown-info" aria-labelledby="navbarDropdownMenuLink">
                                    <form method="post">
                                        <input type="hidden" name="action" value="allChecklistsPrivees"></input>
                                        <input type="submit" value="Toutes les checklists privées" class="dropdown-item waves-effect waves-light">
                                    </form>
                                    <form method="post">
                                        <input type="hidden" name="action" value="allUsers"></input>
                                        <input type="submit" value="Tous les utilisateurs" class="dropdown-item waves-effect waves-light">
                                    </form>
                                </div>
                            </li>';
                            }
                        }
                    }
                    ?>
                </ul>
                <div>
                    <form method="post" class="d-flex">
                        <?php
                        if (!isset($user)) {
                            $user = null;
                        }
                        if ($user == null) {
                            echo '<input type="hidden" name="action" value="signIn"></input>
                                <input type="submit" value="Se connecter / S\'inscrire" class="nav-link btn btn-primary"></input>';
                        } else {
                            echo '<input type="hidden" name="action" value="signOut"></input>
                                <input type="submit" value="Se déconnecter (' . $user->getUserName() . ')" class="nav-link btn btn-primary"></input>';
                        }
                        ?>
                    </form>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <?php
            if (!isset($checklists)) {
                require($dir . $view['erreur_serveur']);
            }
            $counter = 0;
            if (!isset($numberOfCardBeforeWrapping)) {
                $numberOfCardBeforeWrapping = 3;
            }
            $i = 0;


            foreach ($checklists as $list) {
                if ($i % $numberOfCardBeforeWrapping == 0) {
                    if ($i == 0) {
                        echo '<div class="row">';
                    } else {
                        echo '</div><div class="row">';
                    }
                }

                echo '<div class="col">
                <div class="card mt-4">
                <div class="card-header-tab card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal d-flex justify-content-between align-items-center p-0">
                        <div>
                            <i class="fa fa-tasks"></i> ' . $list->getName() . '
                        </div>
                        <div class="d-flex justify-content-between align-items-center p-0">
                        <div class="font-weight-light font-italic">';

                if ($list->getOwner() == null) {
                    echo 'Checklist publique';
                } else {
                    echo 'Auteur : ' . $user->getUserName();
                }
                echo '</div>';
                echo '<form method="post" class="d-flex">
                    <input type="hidden" name="action" value="deletePrivateChecklist"></input>
                    <input type="hidden" name="idChecklist" value="' . $list->getId() . '"></input>
                    <input type="submit" value="" class="bg-transparent border-0 m-0 p-0">
                        <button class="border-0 btn-transition btn btn-outline-danger ml-1"><i class="fa fa-trash"></i></button>
                    </input>
                </form>';
                echo '</div></div></div><div class="card-body mb-3" style="overflow: auto; height: 198pt;"><ul>';

                foreach ($list->getItems() as $item) {
                    echo '<li class="list-group-item">
                        <div class="d-flex align-items-center">
                            <div class="flex-fill">';
                    if ($item->isChecked()) {
                        echo '<div class="text-muted">';
                        if ($item->isTagged()) {
                            echo '<div class="badge badge-' . $item->getTagColor() . ' mr-2">' . $item->getTagText() . '</div>';
                        }
                        echo '<strike>' . $item->getTitle() . '</strike></div>';
                    } else {
                        echo '<div>';
                        if ($item->isTagged()) {
                            echo '<div class="badge badge-' . $item->getTagColor() . ' mr-2">' . $item->getTagText() . '</div>';
                        }
                        echo $item->getTitle() . '</div>';
                    }
                    if ($item->getCreator() == null) {
                        echo '<div class="font-weight-light font-italic"><i>Auteur anonyme</i>';
                    } else {
                        echo '<div class="font-weight-light font-italic"><i>Par ' . $item->getCreator()->getUserName() . '</i>';
                    }
                    $now = new DateTime();
                    $diff = $now->diff($item->getCreationDate());
                    if ($diff->d < 1) {
                        echo '<div class="badge badge-pill badge-primary ml-2">Nouveau</div>';
                    }
                    echo '</div></div>';
                    if ($item->isChecked()) {
                        echo '<form method="post" class="d-flex">
                                        <input type="hidden" name="action" value="uncheckPrivateItem"></input>
                                        <input type="hidden" name="idChecklist" value="' . $list->getId() . '"></input>
                                        <input type="hidden" name="id" value="' . $item->getId() . '"></input>
                                        <input type="submit" value="" class="bg-transparent border-0 form-control">
                                            <button class="border-0 btn-transition btn btn-outline-warning ml-auto"><i class="fas fa-undo-alt"></i></button>
                                        </input>
                                    </form>';
                    } else {
                        echo '<form method="post" class="d-flex">
                                        <input type="hidden" name="action" value="checkPrivateItem"></input>
                                        <input type="hidden" name="idChecklist" value="' . $list->getId() . '"></input>
                                        <input type="hidden" name="id" value="' . $item->getId() . '"></input>
                                        <input type="submit" value="" class="bg-transparent border-0 form-control">
                                            <button class="border-0 btn-transition btn btn-outline-success ml-auto"><i class="fa fa-check"></i></button>
                                        </input>
                                    </form>';
                    }
                    echo '<form method="post" class="d-flex">
                                    <input type="hidden" name="action" value="deletePrivateItem"></input>
                                    <input type="hidden" name="idChecklist" value="' . $list->getId() . '"></input>
                                    <input type="hidden" name="id" value="' . $item->getId() . '"></input>
                                    <input type="submit" value="" class="bg-transparent border-0 m-0 p-0">
                                        <button class="border-0 btn-transition btn btn-outline-danger ml-auto"><i class="fa fa-trash"></i></button>
                                    </input>
                                </form>
                            </div>
                    </li>';
                }

                echo '<li class="list-group-item">
                            <form method="post" class="d-flex align-items-center">
                                <div class="flex-fill">
                                    <input type="hidden" name="action" value="addPrivateItem"></input>
                                    <input type="hidden" name="idChecklist" value="' . $list->getId() . '"></input>
                                    <input type="text" class="form-control" name="title" placeholder="Nom de la nouvelle tâche" required></input>
                                    <input type="text" class="form-control mt-1" name="tagText" placeholder="Nom du tag"></input>
                                    <select class="form-control mt-1" name="tagColor">';
                global $tagColors;
                foreach (array_keys($tagColors) as $color) {
                    echo '<option>' . $color . '</option>';
                }
                echo '</select>
                                </div>
                                <div class="ml-auto">
                                    <input type="submit" value="" class="bg-transparent border-0">
                                        <button class="border-0 btn-transition btn btn-outline-primary ml-auto"><i class="fa fa-plus"></i></button>
                                    </input>
                                </div>
                            </form>
                    </li>';

                echo '</ul></div></div></div>';
                $i++;
            }

            if ($i % $numberOfCardBeforeWrapping == 0) {
                if ($i == 0) {
                    echo '<div class="row">';
                } else {
                    echo '</div><div class="row">';
                }
            }

            echo '<div class="col">
                <div class="card mt-4">
                <div class="card-header-tab card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal d-flex justify-content-between">
                        <div>
                            <i class="fa fa-tasks"></i> Ajouter une nouvelle checklist
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" class="d-flex align-items-center">
                        <div class="flex-fill">
                            <input type="hidden" name="action" value="addPrivateChecklist"></input>
                            <input type="text" class="form-control" name="name" placeholder="Nom de la nouvelle checklist" required></input>
                        </div>
                        <div class="ml-auto">
                            <input type="submit" value="" class="bg-transparent border-0">
                                <button class="border-0 btn-transition btn btn-outline-primary m"><i class="fa fa-plus"></i></button>
                            </input>
                        </div>
                    </form>
                </div>
            </div>
            </div></div>';

            ?>

        </div>
    </div>

    <!-- scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="Ressources/bootstrap/js/bootstrap.min.js"></script>
    <script src="Ressources/particles/particles.js"></script>
    <script src="Ressources/particles/js/app.js"></script>
    <script src="Ressources/sticky.js"></script>

    <script>
        $(document).ready(function() {
            $("").sticky({
                topSpacing: 0
            });
        });
    </script>


</body>

</html>