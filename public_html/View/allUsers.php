<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Gaspard ANDRIEU et Maxime POULAIN">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="Ressources/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Particles.js -->
    <link rel="stylesheet" media="screen" href="Ressources/particles/css/style.css">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="Ressources/fontawesome/css/all.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="Ressources/custom-styles.css">

    <!-- Title -->
    <title>Virtual Lists | Users</title>

    <!-- Icon -->
    <link rel="icon" href="Ressources/images/logo.png" />
</head>

<body class="bodyFade" style="overflow: auto">
    <div id="particles-js" class="position-fixed"></div>
    <div class="sticky-top pb-3">
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <!-- Brand -->
            <a class="navbar-brand" href="">Virtual Lists</a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav flex-grow-1">
                    <li class="nav-item">
                        <form method="post">
                            <input type="hidden" name="action" value="accueil"></input>
                            <input type="submit" value="Accueil" class="bg-transparent border-0 nav-link">
                        </form>
                    </li>
                    <li class="nav-item">
                        <form method="post">
                            <input type="hidden" name="action" value="checklistsPubliques"></input>
                            <input type="submit" value="Checklists publiques" class="bg-transparent border-0 nav-link">
                        </form>
                    </li>
                    <?php
                    if (isset($user)) {

                        if ($user != null) { // L'utilisateur est connecté.
                            echo '<li class="nav-item">
                            <form method="post">
                                <input type="hidden" name="action" value="checklistsPrivees"></input>
                                <input type="submit" value="Vos checklists" class="bg-transparent border-0 nav-link">
                            </form>
                        </li>';
                            if ($user->isAdmin()) { // L'utilisateur est également un administrateur.
                                echo '<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light active" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Accès global<div class="d-inline"></div> <span class="badge badge-danger">Administration</span> </a>
                                <div class="dropdown-menu dropdown-info" aria-labelledby="navbarDropdownMenuLink">
                                    <form method="post">
                                        <input type="hidden" name="action" value="allChecklistsPrivees"></input>
                                        <input type="submit" value="Toutes les checklists privées" class="dropdown-item waves-effect waves-light">
                                    </form>
                                    <form method="post">
                                        <input type="hidden" name="action" value="allUsers"></input>
                                        <input type="submit" value="Tous les utilisateurs" class="dropdown-item waves-effect waves-light">
                                    </form>
                                </div>
                            </li>';
                            }
                        }
                    }

                    ?>
                </ul>
                <div>
                    <form method="post" class="d-flex">
                        <?php
                        if (!isset($user)) {
                            $user = null;
                        }
                        if ($user == null) {
                            echo '<input type="hidden" name="action" value="signIn"></input>
                                <input type="submit" value="Se connecter / S\'inscrire" class="nav-link btn btn-primary"></input>';
                        } else {
                            echo '<input type="hidden" name="action" value="signOut"></input>
                                <input type="submit" value="Se déconnecter (' . $_SESSION['user']->getUserName() . ')" class="nav-link btn btn-primary"></input>';
                        }
                        ?>
                    </form>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <?php
            if (!isset($users) || !isset($infosUsers)) {
                require($dir . $view['erreur_serveur']);
            }
            $counter = 0;
            if (!isset($numberOfCardBeforeWrapping)) {
                $numberOfCardBeforeWrapping =  3;
            }

            $i = 0;
            global $superUser;
            foreach ($users as $rowUser) {
                if ($i % $numberOfCardBeforeWrapping == 0) {
                    if ($i == 0) {
                        echo '<div class="row">';
                    } else {
                        echo '</div><div class="row">';
                    }
                }

                echo '<div class="col">
                    <div class="card mt-4">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal d-flex justify-content-between align-items-center">
                            <div class="font-weight-light font-italic">' . $rowUser->getUserName() . '</div>';
                echo '<form method="post" class="d-flex">';
                if ($superUser == $rowUser->getId() || $user->getUserName() == $rowUser->getUserName()) {
                    echo '<input type="submit" value="" class="bg-transparent border-0 m-0 p-0" disabled>
                    <button class="border-0 btn-transition btn btn-outline-dark ml-auto p-0 m-0" disabled><i class="fa fa-trash"></i></button>
                </input>';
                } else {
                    echo '<input type="hidden" name="action" value="deleteUser"></input>
                    <input type="hidden" name="idUser" value="'.$rowUser->getId().'"></input>
                    <input type="submit" value="" class="bg-transparent border-0 m-0 p-0">
                        <button class="border-0 btn-transition btn btn-outline-danger ml-auto p-0 m-0"><i class="fa fa-trash"></i></button>
                    </input>';
                }
                echo '</form>';

                echo '</div></div><div class="card-body mb-3" style="overflow: auto; height: 198pt;"><ul>';
                echo '<li class="font-size-lg font-weight-normal list-group-item">Nombre de checklists de l\'utilisateur : ' . $infosUsers[$i][0] . '</li>';
                echo '<li class="font-size-lg font-weight-normal list-group-item">Nombre d\'items de l\'utilisateur : ' . $infosUsers[$i][1] . '</li>';
                echo '<li class="font-size-lg font-weight-normal list-group-item">Email : ' . $rowUser->getUserEmail() . '</li>';
                if ($rowUser->isAdmin()) {
                    echo '<li class="font-size-lg font-weight-normal list-group-item">Administrateur : Oui </li>';
                } else {
                    echo '<li class="font-size-lg font-weight-normal list-group-item">Administrateur : Non </li>';
                }

                echo '</ul></div></div></div>';
                $i++;
            }

            if ($i % $numberOfCardBeforeWrapping == 0) {
                if ($i == 0) {
                    echo '<div class="row">';
                } else {
                    echo '</div><div class="row">';
                }
            }

            ?>

        </div>
    </div>

    <!-- scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="Ressources/bootstrap/js/bootstrap.min.js"></script>
    <script src="Ressources/particles/particles.js"></script>
    <script src="Ressources/particles/js/app.js"></script>
    <script src="Ressources/sticky.js"></script>

    <script>
        $(document).ready(function() {
            $("").sticky({
                topSpacing: 0
            });
        });
    </script>

</body>

</html>