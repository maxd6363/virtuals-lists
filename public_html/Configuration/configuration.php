<?php

// Adresse HTTP du site
$httpAdress = "http://51.75.133.250/~progweb";

// Directory du site.
$dir = __DIR__.'/../';

// Liste des modules à inclure
$dConfig["includes"] = array('Controller/Validation.php');

// Connexion à la base de données.
$databaseName = "mysql:host=localhost;dbname=progweb";
$databaseLogin = "progweb";
$databasePassword = "perdu";

// Liste des vues.
$view["authentification"] = "View/authentification.php";
$view["enregistrement"] = "View/enregistrement.php";
$view["accueil"] = "View/accueil.php";
$view["erreur_client"] = "View/erreur404.php";
$view["erreur_serveur"] = "View/erreur500.php";
$view["erreur_sql"] = "View/erreurSQL.php";
$view["checklistsPubliques"] = "View/checklistsPubliques.php";
$view["checklistsPrivees"] = "View/checklistsPrivees.php";
$view["toutesChecklistsPrivees"] = "View/toutesChecklistsPrivees.php";
$view["allUsers"] = "View/allUsers.php";

// Configuration des vues.
$numberOfCardBeforeWrapping = 3;

// Liste des couleurs possibles pour le tag.
$tagColors = array(
    "Aucune couleur" => "white",
    "Bleu" => "primary",
    "Cyan" => "info",
    "Vert" => "success",
    "Jaune" => "warning",
    "Rouge" => "danger",
    "Gris clair" => "light",
    "Gris" => "secondary",
    "Gris foncé" => "dark"
);

// ID du super utilisateur.
$superUser = 1;