<?php


class SplClassLoader
{
    private $_fileExtension = '.php';
    private $_namespace;
    private $_includePath;
    private $_namespaceSeparator = '\\';

    // Créé un nouveau SplClassLoader qui charge les classes pour un namespace donné.
    public function __construct(string $namespace = null, string $includePath = null)
    {
        $this->_namespace = $namespace;
        $this->_includePath = $includePath;
    }

    // Modifie le séparateur de namespace utilisé par les classes du namespace de ce loader.
    public function setNamespaceSeparator(string $separator)
    {
        $this->_namespaceSeparator = $separator;
    }

    // Retourne le séparateur du namespace de ce loader.
    public function getNamespaceSeparator()
    {
        return $this->_namespaceSeparator;
    }

    // Modifie l'include path pour tout les fichiers dans le namespace de ce loader.
    public function setIncludePath(string $includePath)
    {
        $this->_includePath = $includePath;
    }

    // Retourne l'include path des fichiers du namespace de ce loader.
    public function getIncludePath()
    {
        return $this->_includePath;
    }

    // Défini l'extension de fichier utilisée pour le namespace de ce loader.
    public function setFileExtension($fileExtension)
    {
        $this->_fileExtension = $fileExtension;
    }

    // Retourne l'extension de fichier utilisée par le namespace de ce loader.
    public function getFileExtension()
    {
        return $this->_fileExtension;
    }

    // Ajoute ce loader dans la SPL autoload stack.
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    // Retire de loader de la SPL autoload stack.
    public function unregister()
    {
        spl_autoload_unregister(array($this, 'loadClass'));
    }

    // Charge une classe donnée.
    public function loadClass(string $className)
    {
        if (null === $this->_namespace || $this->_namespace.$this->_namespaceSeparator === substr($className, 0, strlen($this->_namespace.$this->_namespaceSeparator))) {
            $fileName = '';
            $namespace = '';
            if (false !== ($lastNsPos = strripos($className, $this->_namespaceSeparator))) {
                $namespace = substr($className, 0, $lastNsPos);
                $className = substr($className, $lastNsPos + 1);
                $fileName = str_replace($this->_namespaceSeparator, DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
            }
            $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . $this->_fileExtension;
            require ($this->_includePath !== null ? $this->_includePath . DIRECTORY_SEPARATOR : '') . $fileName;
        }
    }
}
