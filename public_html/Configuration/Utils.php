<?php

namespace Configuration;

class Utils
{

    public static function tinyIntToBool($tinyInt): bool
    {
        if ($tinyInt == 0) return false;
        return true;
    }
}

?>