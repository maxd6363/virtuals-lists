<?php

use Controller\FrontController;

// Chargement de la configuration
require_once(__DIR__ . "/Configuration/configuration.php");

// Mettre en commentaire si il y a utilisation de namespace.
/*
require_once(__DIR__.'/Configuration/Autoload.php');
Autoload::load();
*/

// Mettre en commentaire si il n'y a pas d'utilisation de namespace.
require_once(__DIR__ . '/Configuration/SplClassLoader.php');
$libraryLoader = new SplClassLoader("Controller", "./");
$libraryLoader->register();
$libraryLoader = new SplClassLoader("Configuration", "./");
$libraryLoader->register();
$libraryLoader = new SplClassLoader("Model", "./");
$libraryLoader->register();
$libraryLoader = new SplClassLoader("Gateway", "./Model");
$libraryLoader->register();

// Chargement de la session, et on ne laisse jamais la variable SESSION["user"] non définie.
session_start();
if(!isset($_SESSION["user"])) {
    $_SESSION["user"] = null;
}


// Passage au front controller.
new FrontController();

?>