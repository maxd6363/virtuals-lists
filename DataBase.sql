-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 09, 2020 at 06:52 PM
-- Server version: 10.1.41-MariaDB-0+deb9u1
-- PHP Version: 7.3.13-1+0~20191218.50+debian9~1.gbp23c2da

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `Tchecklistmembers`
--

CREATE TABLE `Tchecklistmembers` (
  `idChecklist` int(11) NOT NULL,
  `idMember` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Tchecklists`
--

CREATE TABLE `Tchecklists` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `idOwner` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Tchecklists`
--

INSERT INTO `Tchecklists` (`id`, `title`, `idOwner`) VALUES
(52, 'This Is The Second Test', NULL),
(69, 'Projet JavaFX', 15),
(70, 'Projet Python', 15),
(79, 'Tâches IRL', 17),
(82, 'Martin matin', NULL),
(84, 'A faire - IUT', 17),
(85, 'test', 1),
(92, 'hey', 22),
(94, 'Bonjour Monsieur', 29),
(95, 'Autant de Todo liste que l&#39;on veut', 29),
(96, 'Retour à la ligne automatique après 3 todo lists', 29),
(97, 'Une dernière pour la route', 29),
(101, 'test ui', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Titems`
--

CREATE TABLE `Titems` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `isChecked` tinyint(1) NOT NULL DEFAULT '0',
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isTagged` tinyint(1) NOT NULL DEFAULT '0',
  `tagName` varchar(20) DEFAULT NULL,
  `tagColor` varchar(20) DEFAULT NULL,
  `idCreator` int(11) DEFAULT NULL,
  `idChecklist` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Titems`
--

INSERT INTO `Titems` (`id`, `title`, `isChecked`, `creationDate`, `isTagged`, `tagName`, `tagColor`, `idCreator`, `idChecklist`) VALUES
(79, 'Fixer le style du FX:Root en bas', 1, '2019-12-25 23:42:26', 1, '', 'white', 15, 69),
(80, 'Fixer les couleurs', 1, '2019-12-25 23:42:39', 1, '', 'white', 15, 69),
(81, 'Faire le stub des règles', 1, '2019-12-25 23:42:49', 1, '', 'white', 15, 69),
(82, 'Faire le png de victoire / défaite', 1, '2019-12-25 23:43:26', 1, '', 'white', 15, 70),
(83, 'Faire la version console', 0, '2019-12-25 23:43:39', 1, '', 'white', 15, 70),
(84, 'Faire l&#39;IA', 1, '2019-12-25 23:43:48', 1, '', 'white', 15, 70),
(103, 'Passer le code', 1, '2019-12-29 16:15:25', 1, 'Long terme', 'primary', 17, 79),
(104, 'Passeport', 1, '2019-12-29 16:17:33', 1, 'A faire rapidement', 'warning', 17, 79),
(106, 'Appeller Aodyo', 0, '2019-12-29 16:21:39', 1, '', 'white', 17, 79),
(107, 'Réapprovisionner papier et stylo', 1, '2019-12-29 16:25:40', 1, '', 'white', 17, 79),
(111, 'Gestion de projet (rapports + Gantt)', 1, '2020-01-01 17:36:21', 1, '07/01/2020', 'dark', 17, 84),
(112, 'Expression / PPP (rédaction)', 1, '2020-01-01 17:36:54', 1, '06/01/2020', 'dark', 17, 84),
(113, 'Avancer Bacteriol (model)', 0, '2020-01-01 17:40:22', 1, 'Long terme', 'primary', 17, 84),
(115, 'Finir Hearthston-o-tron', 0, '2020-01-01 17:41:52', 1, 'Difficile', 'danger', 17, 84),
(116, 'Finir SuperMineSweeper', 0, '2020-01-01 17:42:22', 1, 'Long', 'warning', 17, 84),
(126, 'Test depuis admin', 0, '2020-01-08 15:05:58', 1, '', 'white', NULL, 82),
(131, 'azeaze', 0, '2020-01-08 15:20:49', 1, '', 'white', 1, 70),
(133, 'test', 0, '2020-01-09 10:43:51', 1, '', 'white', NULL, 82),
(134, 'commenct ca va', 0, '2020-01-09 10:44:13', 1, '', 'white', 22, 92),
(136, 'Voici un test d&#39;item', 0, '2020-01-09 10:50:24', 1, '', 'white', 29, 94),
(137, 'Ici avec un badge', 0, '2020-01-09 10:50:43', 1, 'MON BADGE', 'info', 29, 94),
(138, 'Quelque chose d&#39;urgent', 0, '2020-01-09 10:51:00', 1, 'URGENT', 'danger', 29, 94),
(139, 'J&#39;espère que cela vous plaira', 0, '2020-01-09 10:52:06', 1, '', 'white', 29, 97),
(148, 'Faire les courses', 0, '2020-01-09 18:49:11', 1, 'URGENT', 'danger', NULL, 52),
(149, 'Aller voir mamie', 1, '2020-01-09 18:49:49', 1, '', 'white', NULL, 52),
(150, 'Je suis le super utilisateuuur', 0, '2020-01-09 18:51:02', 1, '', 'white', 1, 85);

-- --------------------------------------------------------

--
-- Table structure for table `Tusers`
--

CREATE TABLE `Tusers` (
  `id` int(11) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `userPassword` varchar(500) NOT NULL,
  `userEmail` varchar(100) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Tusers`
--

INSERT INTO `Tusers` (`id`, `userName`, `userPassword`, `userEmail`, `isAdmin`) VALUES
(1, 'admin', '$2y$10$JT4DDY3y62mNQMzYf0bdo.hdWMgW54/pXXbog6qQstyJNM3QTfO/G', 'admin@gmail.com', 1),
(15, 'Maxd63', '$2y$10$8JDO.7Wr3JcVJuqm4LnE8e2yXd7agJKrzbnmsBHYkSDXkU6jwA7hK', 'poulain.maxou@gmail.com', 1),
(17, 'Gasparino', '$2y$10$Lx4zeJxbPcGSpTnBn4Z80OJwR6Nc74yfjU1bVuM3rGQNxyi6B8gBu', 'skynidor@gmail.com', 1),
(18, 'simpleUser', '$2y$10$jVJrEuPzX72aFlzErEf33eesy5DzId1gn6P19UJqXAlYgQni3TApC', 'test@test.fr', 0),
(22, 'Michel3', '$2y$10$WudYbanjmDh064UxZG2JLup/l4yNeK27eDBBD43RrT3xVwWrOWMoy', 'yoyoyo@gg.comzer', 0),
(29, 'sasa27', '$2y$10$1xZcYwiGONSOzcrzH5GRf.XorN/a.dgKQ5nwCE4cd.QHyLDD9dybi', 'sebastien.salva@uca.fr', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Tchecklistmembers`
--
ALTER TABLE `Tchecklistmembers`
  ADD PRIMARY KEY (`idChecklist`,`idMember`),
  ADD KEY `tchecklistmembers_ref_member` (`idMember`);

--
-- Indexes for table `Tchecklists`
--
ALTER TABLE `Tchecklists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tchecklists_ref_owner` (`idOwner`);

--
-- Indexes for table `Titems`
--
ALTER TABLE `Titems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `titems_ref_checklist` (`idChecklist`),
  ADD KEY `titems_ref_creator` (`idCreator`);

--
-- Indexes for table `Tusers`
--
ALTER TABLE `Tusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tusers_unique_username` (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Tchecklists`
--
ALTER TABLE `Tchecklists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `Titems`
--
ALTER TABLE `Titems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `Tusers`
--
ALTER TABLE `Tusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Tchecklistmembers`
--
ALTER TABLE `Tchecklistmembers`
  ADD CONSTRAINT `tchecklistmembers_ref_checklist` FOREIGN KEY (`idChecklist`) REFERENCES `Tchecklists` (`id`),
  ADD CONSTRAINT `tchecklistmembers_ref_member` FOREIGN KEY (`idMember`) REFERENCES `Tusers` (`id`);

--
-- Constraints for table `Tchecklists`
--
ALTER TABLE `Tchecklists`
  ADD CONSTRAINT `tchecklists_ref_owner` FOREIGN KEY (`idOwner`) REFERENCES `Tusers` (`id`);

--
-- Constraints for table `Titems`
--
ALTER TABLE `Titems`
  ADD CONSTRAINT `titems_ref_checklist` FOREIGN KEY (`idChecklist`) REFERENCES `Tchecklists` (`id`),
  ADD CONSTRAINT `titems_ref_creator` FOREIGN KEY (`idCreator`) REFERENCES `Tusers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
